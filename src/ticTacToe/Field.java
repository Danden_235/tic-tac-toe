package ticTacToe;

import utils.Constant;

public class Field implements FieldInterface {
    private int userSelect;
    public static int[] FIELD = new int[Constant.FIELD_SIZE];
    private int rowLen = Constant.FIELD_SIZE / Constant.ROW_COUNT;


    Field(){
        for (int i = 0; i < FIELD.length;i++){
            FIELD[i] = Constant.EMPTY;
        }
    }

    public void printField() {
        int counter = 0;
        for (int i = 0; i < Constant.ROW_COUNT; i++) {
            for (int j = 0; j < rowLen; j++) {
                switch (FIELD[counter]) {
                    case Constant.CIRCLE:
                        System.out.print(" O");
                        break;
                    case Constant.CROSS:
                        System.out.print(" X");
                        break;
                    default:
                        System.out.print(" _");
                }
                counter++;
            }
            System.out.println();
        }

    }

    public int fillCell(int userSelect, int userMark) {

        this.userSelect = userSelect;
        if (!isRangeCorrect()) {
            return Constant.ERROR_OUT_OF_RANGE_INPUT;
        }
        if (!isCellFree()) {
            return Constant.ERROR_MOVE_FILLED_CELL;
        }
        FIELD[this.userSelect - 1] = userMark;
        return Constant.MOVE_SUCCESSFUL;
    }

    private boolean isCellFree() {
        return FIELD[userSelect - 1] == Constant.EMPTY;
    }

    private boolean isRangeCorrect() {
        return userSelect >= 1 && userSelect <= 9;
    }


}
