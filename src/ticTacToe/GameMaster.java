package ticTacToe;

import utils.Constant;
import utils.Printer;
import utils.PrinterStrings;
import utils.Utills;


public class GameMaster {

    private Player p1 = new Player();
    private Player p2 = new Player();
    private Player activeUser;

    private Field gameField = new Field();
    private Logic logic = new Logic();

    public void gameExecutorFinal(int gameType) {
        System.out.println("Input a Name");
        selectName(gameType);
        System.out.println("Select a mark [1]Cross [2]Circle");
        selectMarks();
        activeUser = firstTurnPlayerIs();
        while (!Constant.IS_GAME_FINISHED) {
            int playerChoice = Constant.ERROR_CHOICE;
            while (!Constant.IS_MOVE_SUCCESFUL) {
                gameField.printField();
                Printer.typeToConsole(PrinterStrings.playerGridSelectTwo);
                try {
                    playerChoice = Integer.parseInt(activeUser.playerMove());
                } catch (NumberFormatException e) {
                    Printer.typeToConsole(PrinterStrings.playerInputIntParseERR);
                }

                Constant.MOVE_RESULT = gameField.fillCell(playerChoice, activeUser.getMark());


                if (Constant.MOVE_RESULT == Constant.ERROR_OUT_OF_RANGE_INPUT) {
                    Printer.typeToConsole(PrinterStrings.ERROR_OUT_OF_RANGE_INPUT);
                    break;
                } else if (Constant.MOVE_RESULT == Constant.ERROR_MOVE_FILLED_CELL) {
                    Printer.typeToConsole(PrinterStrings.ERROR_MOVE_FILLED_CELL);
                    break;
                } else if (Constant.MOVE_RESULT == Constant.MOVE_SUCCESSFUL) {
                    Printer.typeToConsole(PrinterStrings.MOVE_SUCCESSFUL);
                    System.out.println(activeUser.getName() + " made a move in cell " + playerChoice);
                    Constant.IS_MOVE_SUCCESFUL = true;
                    break;
                }
            }
            if (Constant.IS_MOVE_SUCCESFUL) {
                if (logic.isThereWinner()) {
                    System.out.println("Winner is " + activeUser.getName() + "!!!");
                    Constant.IS_GAME_FINISHED = true;
                }
                if (logic.isFieldFull()) {
                    System.out.println(PrinterStrings.gameDraw);
                    Constant.IS_GAME_FINISHED = true;
                }
                Constant.IS_MOVE_SUCCESFUL = false;
                changeActiveUser();
            }
        }
    }

    private void changeActiveUser() {
        activeUser = activeUser == p1 ? p2 : p1;
    }

    private Player firstTurnPlayerIs() {
        if (p1.getMark() == Constant.CROSS) {
            return p1;
        } else {
            return p2;
        }
    }

    private void selectName(int gameType) {
        System.out.println("Player1 insert your name:");
        p1.setName(Utills.readStringFromConsole());
        if (gameType == Constant.PLAYER_VERSUS_PLAYER) {
            System.out.println("Player2 insert your name");
            p2.setName(Utills.readStringFromConsole());
        } else {
            p2 = new Computer();
            p2.setName("Indeed");
        }
    }

    private void selectMarks() {

        int selectedMark = Constant.ERROR_MARK;
        while (!Constant.IS_MARK_RIGHT) {
            try {
                selectedMark = Integer.parseInt(Utills.readStringFromConsole());
                Constant.IS_MARK_RIGHT = true;
            } catch (NumberFormatException e) {
                Printer.typeToConsole(PrinterStrings.menuUserMarkSelectERR);
            }
        }

        p1.setMark(selectedMark);
        if (p1.getMark() == Constant.CIRCLE) {
            p2.setMark(Constant.CROSS);
        } else {
            p2.setMark(Constant.CIRCLE);
        }
    }
}
