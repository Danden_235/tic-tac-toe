package ticTacToe;

public abstract class BasePlayer {

    protected int mark;
    protected String name;

    public abstract int getMark();

    public String getName() {
        return name;
    }

    public abstract String playerMove();

    public abstract void setMark(int userMark);

    public abstract void setName(String name);

}
