package ticTacToe;


public interface FieldInterface{

    //Prints field
    void printField();

    //Fills field Cell with Cross/Circle
    int fillCell(int userSelect, int userMark);
}
