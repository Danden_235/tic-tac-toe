package ticTacToe;

import utils.Constant;

public class Logic {

    public boolean isThereWinner() {
        return checkRow(Field.FIELD[0], Field.FIELD[4], Field.FIELD[8]) ||
                checkRow(Field.FIELD[2], Field.FIELD[4], Field.FIELD[6]) ||
                checkRow(Field.FIELD[0], Field.FIELD[1], Field.FIELD[2]) ||
                checkRow(Field.FIELD[3], Field.FIELD[4], Field.FIELD[5]) ||
                checkRow(Field.FIELD[6], Field.FIELD[7], Field.FIELD[8]) ||
                checkRow(Field.FIELD[0], Field.FIELD[3], Field.FIELD[6]) ||
                checkRow(Field.FIELD[1], Field.FIELD[4], Field.FIELD[7]) ||
                checkRow(Field.FIELD[2], Field.FIELD[5], Field.FIELD[8]);
    }

    private boolean checkRow(int cellOne,int cellTwo,int cellThree){
        if (cellOne == Constant.EMPTY || cellTwo == Constant.EMPTY || cellThree == Constant.EMPTY){
            return false;
        }
        return cellOne == cellTwo && cellOne == cellThree;
    }

    public boolean isFieldFull() {
        for (int i = 0; i < Constant.FIELD_SIZE; i++) {
            if (Field.FIELD[i] == Constant.EMPTY) {
                return false;
            }
        }
        return true;
    }
}
