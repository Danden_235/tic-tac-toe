package ticTacToe;

public interface PlayerInterface {
    String playerMove();
    void swapUserMark();
}
