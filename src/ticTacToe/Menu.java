package ticTacToe;

import utils.Constant;
import utils.Printer;
import utils.PrinterStrings;

import java.io.*;

public class Menu {
    public void GameMenuInit() throws IOException {
        Printer.typeToConsole(PrinterStrings.menuWelcomeMSG);
        Printer.typeToConsole("");
        Printer.typeToConsole(PrinterStrings.playerGridSelectOne);
        Printer.typeToConsole(PrinterStrings.menuButtonOne);
        Printer.typeToConsole(PrinterStrings.menuButtonTwo);

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (!Constant.IS_EXIT) {
            try {
                int userSelect = Integer.parseInt(reader.readLine());
                //I like to write switches)) Kappa
                switch (userSelect) {
                    case Constant.menuSelectNewGame:
                        SelectNewGame();
                        Constant.IS_EXIT = true;
                        break;
                    case Constant.menuSelectExit:
                        Constant.IS_EXIT = true;
                        break;
                    default:
                        System.out.println(PrinterStrings.menuInputERR);
                }
            } catch (NumberFormatException e) {
                System.out.println(PrinterStrings.menuInputERR);
            }
        }
    }

    private void SelectNewGame() throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        GameMaster gameMaster = new GameMaster();

        Printer.typeToConsole(PrinterStrings.menuSelectOpponentMSG);
        while (!Constant.IS_EXIT1) {
            try {
                int userSelect = Integer.parseInt(reader.readLine());
                //I like to write switches)) Kappa
                switch (userSelect) {
                    case Constant.menuSelectPlayerVsPlayer:
                        gameMaster.gameExecutorFinal(Constant.PLAYER_VERSUS_PLAYER);
                        Constant.IS_EXIT1 = true;
                        break;
                    case Constant.menuSelectPlayerVsComputer:
                        gameMaster.gameExecutorFinal(Constant.PLAYER_VERSUS_COMPUTER);
                        Constant.IS_EXIT1 = true;
                        break;
                    default:
                        System.out.println(PrinterStrings.menuInputERR);
                }
            } catch (NumberFormatException e) {
                System.out.println(PrinterStrings.menuInputERR);
            }

        }
    }
}