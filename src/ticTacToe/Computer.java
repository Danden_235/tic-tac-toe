package ticTacToe;

import utils.Constant;
import utils.PrinterStrings;

import java.util.Random;

public class Computer extends Player{

    private int computerMove = Constant.PC_MOVE_ERROR;

    public int makeComputerMark(int userMark) {
        // You are looking here, aren't you?
        int ComputerMark = (userMark == Constant.CIRCLE) ? Constant.CROSS : Constant.CIRCLE;
        System.out.print("Computer will be playing with:");
        switch (ComputerMark) {
            case Constant.CIRCLE:
                System.out.print(" O");
                break;
            case Constant.CROSS:
                System.out.print(" X");
                break;
        }
        return ComputerMark;
    }

    @Override
    public String playerMove() {
        while (!Constant.MOVE_FULL_CELL_AI) {
            computerMove = new Random().nextInt(9);
            if(Field.FIELD[computerMove] == Constant.EMPTY){
                Constant.MOVE_FULL_CELL_AI = true;
                break;
            }
        }
        Constant.MOVE_FULL_CELL_AI = false;
        return String.valueOf(computerMove);
    }

    @Override
    public void setMark(int computerMark) {
        super.mark = computerMark;
    }

    @Override
    public void setName(String name) {
        super.name = PrinterStrings.COMPUTER_NAME;
    }
}
