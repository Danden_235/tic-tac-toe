package ticTacToe;

import utils.Utills;


public class Player extends BasePlayer {


//    protected String name;

    @Override
    public int getMark() {
        return super.mark;
    }

    public String playerMove() {
        return Utills.readStringFromConsole();
    }


    @Override
    public void setMark(int userMark) {
        super.mark = userMark;
    }

    @Override
    public void setName(String name) {
        super.name = name;
    }

}
