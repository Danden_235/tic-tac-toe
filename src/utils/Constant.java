package utils;


public final class Constant {
    //Start menu input constants
    public static final int menuSelectPlayerVsPlayer = 1;
    public static final int menuSelectPlayerVsComputer = 2;
    public static final int menuSelectNewGame = 1;
    public static final int menuSelectExit = 2;
    public static final int menuSelectCircle = 1;
    public static final int menuSelectCross = 2;

    public static final int PLAYER_VERSUS_PLAYER = 0;
    public static final int PLAYER_VERSUS_COMPUTER = 1;

    public static final int ERROR_MARK = 42;
    public static final int ERROR_CHOICE = 42;
    public static final int PC_MOVE_ERROR = 42;
    public static boolean MOVE_FULL_CELL_AI = false;
    public static boolean IS_MARK_RIGHT = false;
    public static boolean IS_EXIT1 = false;
    public static boolean IS_CHOICE_CORRECT = false;

    public static boolean IS_EXIT;
    public static boolean IS_MOVE_SUCCESFUL = false;
    public static boolean IS_GAME_FINISHED = false;

    public static final int FIELD_SIZE = 9;
    public static final int ROW_COUNT = 3;

    public static final int EMPTY = 42;

    public static final int CROSS = 1;
    public static final int CIRCLE = 2;

    //Move cases
    public static final int MOVE_SUCCESSFUL = 0;
    public static final int ERROR_OUT_OF_RANGE_INPUT = 11;
    public static final int ERROR_MOVE_FILLED_CELL = 22;


    public static final int CONVERT_ERROR = 0;

    public static final int WIN_LOGIC_NUMBER = ROW_COUNT + 1;


    public static int MOVE_RESULT = 42;
}
