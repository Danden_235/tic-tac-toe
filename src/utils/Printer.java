package utils;

public class Printer {

    public static void typeToConsole(String s){
        System.out.println(s);
    }

    public static void typeToConsole(int i){
        System.out.print(i);
    }

}
