package utils;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Utills {


    public static int convStringToNumber(String input){
        int returnNumber = 0;
        try {
            returnNumber = Integer.parseInt(input);
        } catch (NumberFormatException e){
            e.printStackTrace();
        }
        return returnNumber;

    }

    public static String readStringFromConsole() {
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        String s = null;
            try {
                s = scan.readLine();
            } catch (IOException e) {
                Printer.typeToConsole(PrinterStrings.playerInputERR1);
            }
        return s;
    }
}
