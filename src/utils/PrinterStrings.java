package utils;

public class PrinterStrings {

    public static final String menuWelcomeMSG = "Welcome to the Tic-Tac-Toe!";
    public static final String menuUserMarkSelectMSG = "Choose who you want to be Player1 : [1]Circle/[2]Cross";
    public static final String menuButtonOne = "[1]  New Game";
    public static final String menuButtonTwo = "[2]    Exit";
    public static final String menuInputERR = "Please insert a integer [1-2]";

    public static final String playerGridSelectOne = "Type a integer [1-2]";
    public static final String playerGridSelectTwo = "Type a integer [1-9]";
    public static final String playerInputERR = "Please insert an integer [1-9]";
    public static final String playerInputIntParseERR = "Please insert an integer";
    public static final String playerInputERR1 = "Please insert a correct input";
    public static final String MOVE_SUCCESSFUL = "Move is Successful!";
    public static final String playerMoveFullCellERR = "Cell is full. Please choose another one";

    public static final String menuUserMarkSelectERR = "Please insert a integer [1-2]";

    public static final String gameDraw = "Draw! You all fools!";
    public static final String winnerIsCross = "Cross wins! Circles - roll out'a here!";
    public static final String winnerIsCircle = "Circle wins! Crosses - mark the other spot, this spot is TAKEN!";


    public static final String winner = "Winner is ";
    public static final String menuSelectOpponentMSG = "Choose an opponent: [1]Player/[2]Computer";
    public static final String COMPUTER_NAME = "Skynet";
    public static final String ERROR_OUT_OF_RANGE_INPUT = "Please insert an integer in range between 1 & 9";
    public static final String ERROR_MOVE_FILLED_CELL = "FUCK YOURSELF - CELL IS FILLED!";
}
